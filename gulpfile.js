var gulp = require('gulp'),
	browserify = require('browserify'),
	vinylSource = require('vinyl-source-stream'),
	sass = require('gulp-sass'),
	del = require('del'),
	gutil = require('gutil');

var DEST = 'dist/';
var watches = [
	{
		name: 'js',
		files: 'app/**/*.js',
		tasks: ['js']
	},
	{
		name: 'sass',
		files: 'app/**/*.sass',
		tasks: ['sass']
	},
	{
		name: 'assets',
		files: 'assets/**',
		tasks: ['assets']
	},
	{
		name: 'index',
		files: 'app/index.html',
		tasks: ['index']
	}
];
var toWatch = [];

gulp.task(
	'js',
	function () {
		return browserify('./app/app.js', {debug: true})
			.bundle()
			.on('error', function (e) {
				gutil.log(e);
			})
			.pipe(vinylSource('bundle.js'))
			.pipe(gulp.dest(DEST + 'js/'));
	}
);

gulp.task(
	'sass',
	function () {
		return gulp
			.src('./sass/main.sass')
			.pipe(sass('main.css').on('error', sass.logError))
			.pipe(gulp.dest(DEST + 'css/'));
	}
);
gulp.task(
	'index',
    function () {
	    return gulp
		    .src('./app/index.html')
		    .pipe(gulp.dest(DEST));
    }
);
gulp.task(
	'assets',
    function () {
	    return gulp
		    .src('./assets/**')
		    .pipe(gulp.dest(DEST + 'assets/'));
    }
);
gulp.task(
	'clean',
    function (cb) {
	    del('dist/**');
	    cb();
    }
);
gulp.task('build', ['clean', 'js', 'sass', 'index', 'assets']);
//todo find a fix for errors on watches
watches.forEach(
	function (watch) {
		var name;

		name = 'watch:' + watch.name;

		toWatch.push(name);

		gulp.task(
			name,
		    function () {
			    gulp.watch(watch.files, watch.tasks);
		    }
		)
	}
);
gulp.task('watches', toWatch);